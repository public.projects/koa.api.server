module.exports = {
  root: true,
  env: {
    commonjs: true,
    es6     : true,
    node    : true
  },
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint'
  ],
  extends: [
    'eslint:recommended',
    'plugin:import/recommended'
  ],
  globals: {
    app: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  overrides: [
    {
      files: ['*.js', '*.ts'],
      rules: {
        // styling
        'quote-props': ['error', 'as-needed', { keywords: false, unnecessary: true, numbers: false }],
        'object-property-newline': ['error', { allowAllPropertiesOnSameLine: true }],
        'max-len': ['error', 120, 2, {
          ignoreUrls: true,
          ignoreComments: false,
          ignoreRegExpLiterals: true,
          ignoreStrings: true,
          ignoreTemplateLiterals: true
        }],
        indent: ['error', 2, {
          SwitchCase : 1,
          VariableDeclarator: 1,
          outerIIFEBody: 1,
          FunctionDeclaration: { parameters: 1, body: 1 },
          FunctionExpression: { parameters: 1, body: 1 },
          CallExpression: { arguments: 1 },
          ArrayExpression: 1,
          ObjectExpression: 1,
          ImportDeclaration: 1,
          flatTernaryExpressions: false,
          ignoreComments: false
        }],
        quotes: ['error', 'single'],
        semi: ['error', 'always'],
        'comma-spacing': ['error', { before: false, after: true }],
        'comma-dangle': ['error', 'never'],
        'object-curly-spacing': ['error', 'always'],
        'arrow-parens': ['error', 'always'],
        'array-bracket-spacing': ['error', 'never'],
        'space-before-function-paren': ['error', 'always'],
        'no-spaced-func': 'error',
        'space-infix-ops': 'error',
        'space-in-parens': ['error', 'never'],
        'object-curly-newline': 'off',
        'dot-notation': ['error', { allowPattern: '_' }],
        'eol-last': ['error', 'always'],
        'no-multiple-empty-lines': ['error', { max: 1, maxBOF: 0, maxEOF: 1 }],
        // 'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: false }],
        'lines-between-class-members': 'off',

        // other
        'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
        'no-param-reassign': ['error', { props: false }],
        'max-classes-per-file': 'error',
        'no-shadow': 'off',
        'no-void': 'error',
        'no-unused-vars': 'off',
        'no-use-before-define': ['error', { functions: false }],
        'prefer-template': 'error',
        'no-dupe-keys': 'error',
        'prefer-promise-reject-errors': 'off',
        'no-dupe-class-members': 'off',

        'no-console' : process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

        // import
        'import/named': 'error',
        'import/default': 'error',
        'import/export': 'error',
        'import/no-dynamic-require': 'error',
        'import/no-unresolved': 'off',
        'import/namespace': 'off',
        'import/no-named-as-default-member': 'off',

        '@typescript-eslint/no-shadow': 'error',
        '@typescript-eslint/no-unused-vars': 'error',
        '@typescript-eslint/no-dupe-class-members': 'error'
      }
    }
  ]
};
