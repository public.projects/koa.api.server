const fs = require('fs').promises;
const path = require('path');
const { spawn } = require('child_process');
const dts = require('dts-bundle');
const rimraf = require('rimraf');

const DIST_DIR = path.join(__dirname, './dist');

(async () => {
  console.log('🔔 Removing dist dir...');
  await clearDist();

  console.log('🔔 Building sources...');
  await buildTs();

  console.log('🔔 Bundling d.ts file...');
  await bundleDts();

  console.log('🔔 Clearing dest dir...');
  await clearDts();

  console.log('🔔 Done!!!');
})();

// helpers
async function clearDist () {
  rimraf.sync(DIST_DIR);
}

function buildTs () {
  const tsc      = path.join(__dirname, 'node_modules/.bin/tsc');
  const tsConfig = path.join(__dirname, './tsconfig.json');

  return new Promise((resolve, reject) => {
    const ls = spawn(tsc, ['-p', tsConfig]);

    ls.stdout.on('data', (data) => {
      console.log(`⚡️ stdout: ${data}`);
    });

    ls.stderr.on('data', (data) => {
      console.log(`⚡️ stderr: ${data}`);
    });

    ls.on('error', (error) => {
      console.log(`⚡️ error: ${error.message}`);
    });

    ls.on('close', (code) => {
      if (code === 0) {
        resolve();
      } else {
        console.log(`⚡️ process exited with code ${code}`);
        reject();
      }
    });
  });
}

async function bundleDts () {
  dts.bundle({
    name: 'koa.api.server',
    main: 'dist/index.d.ts'
  });
}

async function clearDts (dir = DIST_DIR) {
  try {
    const files = await fs.readdir(dir);

    for (const file of files) {
      const fPath = path.join(dir, file);

      const stat = await fs.lstat(fPath);

      if (stat.isDirectory()) {
        await clearDts(fPath);
      } else if (path.extname(fPath) === '.ts' && file !== 'koa.api.server.d.ts') {
        await fs.unlink(fPath);
        console.log(`⚡️ ${file} - deleted.`);
      }
    }
  } catch(err) {
    console.log(err);
  }
}
