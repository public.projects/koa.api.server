import { randomUUID } from 'crypto';

export class TracedApiException extends Error {

  readonly traceId: string;

  constructor (message: string) {
    super(message);

    this.name = this.constructor.name;
    this.traceId = randomUUID();

    Error.captureStackTrace(this, this.constructor);
  }

  toJSON () {
    return {
      traceId: this.traceId,
      name   : this.name,
      message: this.message,
      stack  : this.stack?.split('\n')
    };
  }
}
