export interface ILogger {
  info (message: any): void;
  error (err: any): void;
}
