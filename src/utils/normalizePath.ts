export function normalizePath (path: string): string {
  let prefix = path;

  if (prefix.charAt(prefix.length - 1) === '/') {
    prefix = prefix.substring(0, prefix.length - 1);
  }
  if (prefix.charAt(0) !== '/') {
    prefix = `/${prefix}`;
  }

  return prefix;
}
