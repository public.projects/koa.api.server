import crypto from 'crypto';

interface ICryptoOptions {
  algorithms: string;
  password: string;
  vector: string;
}

export class Crypto {
  private algorithms: string;
  private password: string;
  private vector: string;

  constructor (options: ICryptoOptions) {
    if (!options.algorithms) {
      throw new Error('Crypto algorithms required');
    }
    if (!options.password) {
      throw new Error('Crypto password required');
    }
    if (!options.vector) {
      throw new Error('Crypto vector required');
    }

    this.algorithms = options.algorithms;
    this.password = options.password;
    this.vector = options.vector;
  }

  encrypt<T> (data: T): string {
    const { algorithms, password, vector } = this;

    const text   = JSON.stringify(data);
    const cipher = crypto.createCipheriv(algorithms, Buffer.from(password, 'hex'), Buffer.from(vector, 'hex'));

    return Buffer.concat([cipher.update(text), cipher.final()]).toString('hex');
  }

  decrypt<T> (text: string): T {
    const { algorithms, password, vector } = this;

    const encrypted = Buffer.from(text, 'hex');
    const decipher  = crypto.createDecipheriv(algorithms, Buffer.from(password, 'hex'), Buffer.from(vector, 'hex'));
    const decrypted = Buffer.concat([decipher.update(encrypted), decipher.final()]).toString();

    return JSON.parse(decrypted) as T;
  }
}
