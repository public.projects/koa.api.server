type QueryType = Record<string, string | string[] | undefined>;
type ResultType = Record<string, string | number | null | boolean | string[] | number[]>;

export function queryParser (query: QueryType, parseBlacklist: string[] = []): ResultType {
  const parsed: ResultType = {};

  for (const queryKey in query) {
    const isArrayNotation = queryKey.search(/\[]/) > -1;

    let key = isArrayNotation ? queryKey.replace('[]', '') : queryKey;

    let value = query[key];

    const shouldParse = parseBlacklist.indexOf(key) === -1;

    switch (true) {
      case shouldParse && Array.isArray(value):
        parsed[key] = parseArray(value as string[]);
        break;
      case shouldParse && typeof value === 'string':
        if (value === 'null') {
          parsed[key] = null;
        } else {
          parsed[key] = isArrayNotation
            ? parseArray([value as string])
            : parseValue(value as string);
        }
        break;
      case typeof  value === 'undefined':
        break;
      default:
        parsed[key] = value as string;
        break;
    }
  }

  return parsed;
}

// helpers
function parseValue (value: string): string | number | boolean {
  switch (true) {
    case isInteger(value): return strToInteger(value);
    case isFloat(value)  : return strToFloat(value);
    case isBoolean(value): return strToBoolean(value);
    default              : return value;
  }
}

function parseArray (arr: string[]): string[] | number[] {
  switch (true) {
    case arr.every(isInteger): return arr.map(strToInteger) as number[];
    case arr.every(isFloat)  : return arr.map(strToFloat)   as number[];
    default                  : return arr.map(String)       as string[];
  }
}

function isInteger (str: string): boolean {
  return str.search(/^-*\d+$/) > -1;
}

function isFloat (str: string): boolean {
  return str.search(/^-*\d+\.\d+$/) > -1;
}

function isBoolean (str: string): boolean {
  return str === 'true' || str === 'false';
}

function strToInteger (val: string): number | string {
  const v = parseInt(val, 10);

  return String(v) === val ? v : val;
}

function strToFloat (val: string): number | string {
  const v = parseFloat(val);

  return String(v) === val ? v : val;
}

function strToBoolean (val: string): boolean {
  return val === 'true';
}
