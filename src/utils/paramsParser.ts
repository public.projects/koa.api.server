type ParamsType = Record<string, string>;
type ResultType = Record<string, string | number>;

export function paramsParser (params: ParamsType, parseBlacklist: string[] = []): ResultType {
  const parsed: ResultType = {};

  for (const key in params) {
    const value = params[key];

    const shouldParse = value.match(/^\d+$/) && parseBlacklist.indexOf(key) === -1;

    parsed[key] = shouldParse
      ? parseInt(value, 10)
      : value;
  }

  return parsed;
}
