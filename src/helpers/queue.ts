import { ILogger } from '../types/interfaces';
import { Queue, QueueOptions } from '../lib';

type InitQueueSettings<T> = { [key in keyof T]: Omit<QueueOptions, 'logger' | 'name'> };
type InitQueueResult<T> = { [key in keyof T]: Queue };

export function initQueue<
  T extends Record<string, Queue>
> (settings: InitQueueSettings<T>, logger: ILogger): InitQueueResult<T> {
  const queue: Partial<InitQueueResult<T>> = {};

  if (!settings) {
    throw new TypeError('Queue settings is required');
  }

  for (const name of Object.keys(settings)) {
    queue[name as keyof T] = new Queue({ name, ...settings[name], logger });
  }

  return queue as InitQueueResult<T>;
}
