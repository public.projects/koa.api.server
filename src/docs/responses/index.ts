type ResponseType = {
  description: string;
};

const responses: Record<number, ResponseType> = {
  204: { description: 'OK' },
  400: { description: 'Bad request' },
  401: { description: 'Unauthorized' },
  403: { description: 'Forbidden' },
  404: { description: 'Not found' },
  409: { description: 'Too many requests' }
};

export default responses;
