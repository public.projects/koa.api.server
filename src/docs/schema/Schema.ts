export interface ISchema {
  type: string;
  isRequired: boolean;
  isNullable: boolean;
  summary: string | null;

  toJSON: () => Object
}

export interface JsonResult {
  type: string;
  required?: true | string[];
  nullable?: true;
  description?: string;
}

export default class Schema implements ISchema {
  type: string;

  isRequired: boolean;

  isNullable: boolean;

  summary: string | null;

  constructor (type: string) {
    this.type       = type;
    this.isRequired = false;
    this.isNullable = false;
    this.summary    = null;
  }

  required () {
    this.isRequired = true;

    return this;
  }

  nullable () {
    this.isNullable = true;

    return this;
  }

  info (text: string) {
    this.summary = text;

    return this;
  }

  toJSON (): JsonResult {
    const result: JsonResult = {
      type    : this.type,
      required: this.isRequired ? true : undefined,
      nullable: this.isNullable ? true : undefined
    };

    if (this.summary) {
      result.description = this.summary;
    }

    return result;
  }
}
