export interface RefJsonResult {
  $ref: string;
}

export default class RefSchema {
  private ref: string;

  constructor (ref: string) {
    this.ref = ref;
  }

  toJSON (): RefJsonResult {
    return {
      $ref: `#/components/schemas/${this.ref}`
    };
  }
}
