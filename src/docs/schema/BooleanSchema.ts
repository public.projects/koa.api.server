import Schema from './Schema';

export default class BooleanSchema extends Schema {
  constructor () {
    super('boolean');
  }
}
