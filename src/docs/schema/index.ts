import { ISchema } from './Schema';
import StringSchema from './StringSchema';
import NumberSchema from './NumberSchema';
import BooleanSchema from './BooleanSchema';
import ObjectSchema, { ObjectSchemaInput } from './ObjectSchema';
import ArraySchema from './ArraySchema';
import RefSchema from './RefSchema';

export default {
  ref    : (ref: string) => new RefSchema(ref),
  string : () => new StringSchema(),
  number : () => new NumberSchema(),
  boolean: () => new BooleanSchema(),
  object : <T extends ObjectSchemaInput>(schema: T) => new ObjectSchema<T>(schema),
  array  : <T extends ISchema>(item: T) => new ArraySchema<T>(item)
};
