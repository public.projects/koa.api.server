import Schema, { JsonResult } from './Schema';

export interface NumberJsonResult {
  minimum?: number;
  maximum?: number;
  enum?: number[];
}

export default class NumberSchema extends Schema {
  minimum?: number;

  maximum?: number;

  enum?: number[];

  constructor () {
    super('number');
  }

  integer () {
    this.type = 'integer';

    return this;
  }

  min (minimum: number) {
    this.minimum = minimum;

    return this;
  }

  max (maximum: number) {
    this.maximum = maximum;

    return this;
  }

  enums (values: any[]) {
    values.forEach((val) => {
      if (this.type === 'integer' && !isInteger(val)) {
        throw new Error(`Should be an integer '${val}'`);
      }
      if (this.type === 'number' && !isNumber(val)) {
        throw new Error(`Should be a number '${val}'`);
      }
    });

    this.enum = values;

    return this;
  }

  toJSON (): NumberJsonResult & JsonResult {
    const fields: Array<keyof NumberJsonResult> = ['minimum', 'maximum', 'enum'];

    return {
      ...super.toJSON(),
      ...fields.reduce((res, field) => {
        const value = this[field];

        if (typeof value !== 'undefined') {
          (res as any)[field] = value;
        }

        return res;
      }, {} as NumberJsonResult)
    };
  }
}

// helpers
function isNumber (val: any): boolean {
  return typeof val === 'number' && Number.isFinite(val);
}

function isInteger (val: any): boolean {
  return isNumber(val) && Number.isInteger(val);
}
