import Schema, { ISchema } from './Schema';

export type ObjectSchemaInput = { [key: string]: ISchema };

export interface ObjectJsonResult<U extends ObjectSchemaInput> {
  type: string;
  required?: string[];
  nullable?: true;
  properties?: { [K in keyof U]: ReturnType<U[K]['toJSON']> }
  additionalProperties?: false;
}

export default class ObjectSchema<T extends ObjectSchemaInput> extends Schema {
  schema: T;

  constructor (schema: T) {
    super('object');

    this.schema = schema;
  }

  getRequiredFields (): string[] {
    return Object.keys(this.schema).reduce((res, key) => {
      const prop = this.schema[key];

      if (prop.isRequired) {
        res.push(key);
      }

      return res;
    }, [] as string[]);
  }

  formProps (): { [K in keyof T]: ReturnType<T[K]['toJSON']> } {
    return Object.keys(this.schema)
      .reduce((res, key) => {
        const props = this.schema[key].toJSON();

        delete (props as any).required;

        return { ...res, [key]: props };
      }, {} as { [K in keyof T]: ReturnType<T[K]['toJSON']> });
  }

  toJSON (): ObjectJsonResult<T> {
    const result: ObjectJsonResult<T> = {
      type: this.type
    };

    const required   = this.getRequiredFields();
    const properties = this.formProps();

    if (required.length) {
      result.required = required;
    }
    if (Object.keys(properties).length) {
      result.properties = properties;
      result.additionalProperties = false;
    }
    if (this.isNullable) {
      result.nullable = true;
    }

    return result;
  }
}
