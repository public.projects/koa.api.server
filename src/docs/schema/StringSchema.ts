import Schema, { JsonResult, ISchema } from './Schema';

export interface StringJsonResult {
  format?: 'uri' | 'email' | 'uuid';
  minLength?: number;
  maxLength?: number;
  pattern?: string;
  enum?: string[];
}

export default class StringSchema extends Schema implements ISchema {
  format?: 'uri' | 'email' | 'uuid';

  minLength?: number;

  maxLength?: number;

  enum?: string[];

  pattern?: string;

  constructor () {
    super('string');
  }

  url () {
    this.format = 'uri';

    return this;
  }

  email () {
    this.format = 'email';

    return this;
  }

  uuid () {
    this.format = 'uuid';

    return this;
  }

  min (length: number) {
    this.minLength = length;

    return this;
  }

  max (length: number) {
    this.maxLength = length;

    return this;
  }

  length (length: number) {
    this.minLength = length;
    this.maxLength = length;

    return this;
  }

  matches (pattern: string) {
    this.pattern = pattern;

    return this;
  }

  enums (values: string[]) {
    values.forEach((val) => {
      if (!isString(val)) {
        throw new Error(`Should be a string '${val}'`);
      }
    });

    this.enum = values;

    return this;
  }

  toJSON (): StringJsonResult & JsonResult {
    const fields: Array<keyof StringJsonResult> = ['format', 'minLength', 'maxLength', 'pattern', 'enum'];

    return {
      ...super.toJSON(),
      ...fields.reduce((res, field) => {
        const value = this[field];

        if (typeof value !== 'undefined') {
          (res as any)[field] = value;
        }

        return res;
      }, {} as StringJsonResult)
    };
  }
}

// helpers
function isString (val: string): boolean {
  return typeof val === 'string';
}
