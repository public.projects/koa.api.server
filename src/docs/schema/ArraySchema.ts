import Schema, { JsonResult, ISchema } from './Schema';

export interface ArrayJsonResult<T> {
  items: T;
  minItems?: number;
  maxItems?: number;
}

export default class ArraySchema<T extends ISchema> extends Schema {
  item: T;

  minItems?: number;

  maxItems?: number;

  constructor (item: T) {
    super('array');

    this.item = item;
  }

  min (minItems: number) {
    this.minItems = minItems;

    return this;
  }

  max (maxItems: number) {
    this.maxItems = maxItems;

    return this;
  }

  toJSON (): ArrayJsonResult<T> & JsonResult {
    return {
      ...super.toJSON(),
      items   : this.item,
      minItems: typeof this.minItems !== 'undefined' ? this.minItems : undefined,
      maxItems: typeof this.maxItems !== 'undefined' ? this.maxItems : undefined
    };
  }
}
