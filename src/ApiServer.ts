import * as events from 'events';
import * as http from 'http';
import moment from 'moment';
import Koa from 'koa';
import Router from 'koa-router';
import prettyMs from 'pretty-ms';
import { initInitialMiddleware } from './middlewares';
import { Notifier, RouteController, QueueController } from './lib';
import { BaseExecutionContext, AppAuthenticators, ContextQueues } from './types';
import { ILogger } from './types/interfaces';

const { version: VERSION } = require('../../package.json');

import 'moment-timezone';

enum ServerState {
  Created = 0,
  Initiated = 1,
  Started = 2,
  Stopped = 3
}

type BaseServerExecutionContext = BaseExecutionContext & {
  readonly notifier?: Notifier;
  readonly queue?: ContextQueues;
}

export interface ServerOptions {
  name: string;
  version: string;
  timezone: string;
  port: number;
  isDevEnv: boolean;
  logger: ILogger;
}

export interface ServerInitOptions<ServerExecutionContext extends BaseServerExecutionContext> {
  middlewares?: Koa.Middleware[];
  authenticators?: AppAuthenticators;
  context: ServerExecutionContext;
}

export class ApiServer<ExecutionContext extends BaseServerExecutionContext> extends events.EventEmitter {

  private state: ServerState = ServerState.Created;
  readonly isDevEnv: boolean;
  readonly createdAt: Date = new Date();
  readonly name: string = 'koa.api.server';
  readonly version: string;
  readonly prefix: string;
  readonly timezone: string;
  readonly port: number;

  private readonly app: Koa;
  private readonly server: http.Server;
  private readonly router: Router;
  private readonly logger: ILogger;

  private authenticators: AppAuthenticators = {};
  private context?: ExecutionContext;

  private routeControllers: Map<string, RouteController<ExecutionContext>> = new Map();
  private queueControllers: Map<string, QueueController<ExecutionContext>> = new Map();

  constructor (options: ServerOptions) {
    super();

    this.setMaxListeners(100);

    this.isDevEnv = options.isDevEnv;
    this.logger   = options.logger;

    this.prefix   = options.name;
    this.version  = options.version;
    this.timezone = options.timezone;
    this.port     = options.port;

    this.app       = new Koa();
    this.app.proxy = true;
    this.server    = http.createServer(this.app.callback());
    this.router    = new Router();
  }

  init (options: ServerInitOptions<ExecutionContext>) {
    if (!this.isCreated) {
      throw new TypeError(`Cannot init server: server is in "${this.state}" state`);
    }
    if (!options.context) {
      throw new TypeError('Context required');
    }

    this.app.use(initInitialMiddleware('koa.api.server', this.version));

    if (options.middlewares && options.middlewares.length) {
      options.middlewares.forEach((middleware) => {
        this.app.use(middleware);
      });
    }

    if (options.authenticators) {
      this.authenticators = options.authenticators;
    }

    this.context = options.context;
    this.state = ServerState.Initiated;

    this.dispatch('initialized');
  }

  async start () {
    if (!this.isInitiated) {
      throw new TypeError('Cannot start server: server should be initiated');
    }
    if (!this.routeControllers.size) {
      throw new TypeError('Cannot start server: no controller has been registered');
    }

    this.dispatch('starting');

    if (this.context?.notifier) {
      this.context.notifier.connect(this.server);
    }

    this.attachRouteControllers();
    this.attachQueueControllers();
    // this.attachSocketControllers(); // todo

    this.app.use(this.router.routes());

    this.server.listen(this.port, () => {
      this.logger.info(`Time: ${moment().tz(this.timezone).format()}`);
      this.logger.info(`Launched in: ${prettyMs(Date.now() - Number(this.createdAt))}`);
      this.logger.info(`Environment: ${this.context!.env}`);
      this.logger.info(`Process PID: ${process.pid}`);
      this.logger.info(`Engine version: ${VERSION} (node v${process.versions.node})`);
      this.logger.info(`App version: ${this.version}`);
      this.logger.info(`⚡️ Server: http://localhost:${this.port}\n`);

      if (this.isDevEnv) {
        this.logger.info('To shut down your server, press <CTRL> + C at any time\n');
      }

      this.state = ServerState.Started;

      this.dispatch('started');
    });
  }

  async stop () {
    if (!this.isStarted) {
      throw new TypeError('Cannot stop server: server has not been started');
    }

    if (this.server) {
      this.server.close();
    }

    if (this.context?.queue) {
      for (const queueName in this.context.queue) {
        this.context.queue[queueName].close();
      }
    }

    this.state = ServerState.Stopped;

    this.dispatch('stopped');
  }

  attachRawRoute (route: Router) {
    this.router.use(route.routes(), route.allowedMethods());
  }

  register (root: string, controller: RouteController<ExecutionContext>): void;
  register (queueName: string, handler: QueueController<ExecutionContext>): void;
  register (target: string, controller: RouteController<ExecutionContext> | QueueController<ExecutionContext>): void {
    if (!this.isInitiated) {
      throw new TypeError(`Cannot register controller: server is in "${this.state}" state`);
    }

    if (controller instanceof RouteController) {
      const root = target;

      if (!controller) {
        throw new TypeError('Cannot register controller: router is undefined');
      }
      if (this.routeControllers.has(target)) {
        throw TypeError(`Path '${root}' has already been attached to a router`);
      }

      this.routeControllers.set(root, controller);

      this.dispatch(`route:${root}:registered`);
    } else if (controller instanceof QueueController) {
      const queueName = target;

      if (!this.context || !this.context.queue || !this.context.queue[queueName]) {
        throw TypeError(`Cannot register queue controller: Queue "${queueName}" is absent in context`);
      }

      if (!controller) {
        throw new TypeError('Cannot register queue controller: controller is undefined');
      }

      if (this.queueControllers.has(queueName)) {
        throw TypeError(`Controller for "${queueName}" queue has already been registered`);
      }

      this.queueControllers.set(queueName, controller);
      this.dispatch(`queue:${queueName}:registered`);
    }
  }

  dispatch (event: string, message?: string | Object | Error): void {
    const eventName = `${this.prefix}:${event}`;

    this.logger.info(`🔔 ${eventName}`);
    this.emit(eventName, message);
  }

  private get isCreated () {
    return this.state === ServerState.Created;
  }

  private get isInitiated () {
    return this.state === ServerState.Initiated;
  }

  private get isStarted () {
    return this.state === ServerState.Started;
  }

  private attachRouteControllers () {
    for (const [root, controller] of this.routeControllers) {
      controller.attach(root, this.router, this.context!, this.authenticators);
      this.logger.info(`⚡️ ${root} route: attached controller`);
    }
  }

  private attachQueueControllers () {
    for (const [queueName, controller] of this.queueControllers) {
      if (this.context && this.context.queue && this.context.queue[queueName]) {
        controller.attach(this.context.queue[queueName], this.context);
        this.logger.info(`⚡️ ${queueName} queue: attached controller`);
      }
    }
  }
}
