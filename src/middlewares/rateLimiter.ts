import rateLimit from 'koa-ratelimit';
import { KoaContext, KoaMiddleware } from '../types';

const db = new Map();

export interface RateLimiterOptions {
	window?: number;
	limit?: number;
	whitelist?: (context: KoaContext) => boolean | Promise<boolean>;
	id?: (context: KoaContext) => string | false;
}

export function initRateLimiter (options: RateLimiterOptions = {}): KoaMiddleware {
  const {
    window,
    limit,
    whitelist = () => false,
    id = (ctx) => ctx.ip
  } = options;

  return rateLimit({
    driver       : 'memory',
    db           : db,
    errorMessage : 'Too many requests',
    id           : id,

    headers      : {
      remaining: 'Rate-Limit-Remaining',
      reset    : 'Rate-Limit-Reset',
      total    : 'Rate-Limit-Total'
    },

    duration     : window,
    max          : limit,

    disableHeader: false,
    whitelist    : whitelist
  });
}
