import koaTooBusy from 'koa-toobusy';
import { KoaMiddleware } from '../types';

export function initTooBusy (options: koaTooBusy.KoaTooBusyOptions = {}): KoaMiddleware {
  return koaTooBusy(options);
}
