import { KoaContext, KoaNext, KoaMiddleware } from '../types';

export function initContentLengthLimiter (maxSizeKb: number): KoaMiddleware {
  const maxSizeBite = maxSizeKb * 1000;

  return async (ctx: KoaContext, next: KoaNext) => {
    const len = ctx.req.headers['content-length'] || ctx.req.headers['Content-length'] || ctx.req.headers['Content-Length'] || '0';

    const bodySize = parseInt(Array.isArray(len) ? len[0] : len, 10);

    if (bodySize > maxSizeBite) {
      ctx.throw(403, `Request body size exceeded (${bodySize} byte).`);
      return;
    }

    await next();
  };
}
