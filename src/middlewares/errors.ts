import _ from 'lodash';
import { Context, Next, Middleware } from 'koa';

interface ErrorMiddlewareOptions {
	isDevEnv: boolean;
}

const DEFAULT_OPTIONS: Required<ErrorMiddlewareOptions> = {
  isDevEnv: false
};

export default function initErrors (options?: ErrorMiddlewareOptions): Middleware {
  const { isDevEnv = true } = { ...DEFAULT_OPTIONS, ...(options || {}) };

  return async (ctx: Context, next: Next) => {
    try {
      await next();
    } catch (error) {
      ctx.status = _.get(error, 'status', 500);

      if (ctx.status !== 403) {
        ctx.body = {
          error: _.get(error, 'message', 'Internal Server Error')
        };

        if (isDevEnv) {
          let stack: string = _.get(error, 'stack', '').toString();

          (ctx.body as any).stack = stack ? stack.split('\n').slice(0, 6) : [];
        }
      }

      throw error;
    }
  };
}
