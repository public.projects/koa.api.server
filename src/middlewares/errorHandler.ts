import _ from 'lodash';
import { KoaContext, KoaNext, KoaMiddleware } from '../types';
import { TracedApiException } from '../errors';

interface IErrorParams {
  status: number;
  code: number;
  message?: string;
}

interface ErrorMiddlewareOptions {
	isDevEnv: boolean;
  apiErrors: Map<string, IErrorParams>;
}

const DEFAULT_OPTIONS: Required<ErrorMiddlewareOptions> = {
  isDevEnv : false,
  apiErrors: new Map()
};

export function initErrorHandler (options?: ErrorMiddlewareOptions): KoaMiddleware {
  const { isDevEnv, apiErrors } = { ...DEFAULT_OPTIONS, ...(options || {}) };

  return async (ctx: KoaContext, next: KoaNext) => {
    try {
      await next();
    } catch (error) {
      if (error instanceof TracedApiException) {
        const params = apiErrors.get(error.name);

        ctx.status = _.get(params, 'status', _.get(error, 'status', 500));

        ctx.body = {
          ...error.toJSON(),
          status : ctx.status,
          code   : _.get(params, 'code', -1)
        };
      } else {
        ctx.status = _.get(error, 'status', 500);

        ctx.body = {
          name   : _.get(error, 'name', 'Error'),
          message: _.get(error, 'message', 'Internal Server Error'),
          status : ctx.status,
          stack  : _.get(error, 'stack', 'Unknown stack').split('\n')
        };
      }

      if (ctx.status === 403) {
        ctx.body = _.pick(ctx.body as object, ['status']);
      }
      if (!isDevEnv) {
        ctx.body = _.omit(ctx.body as object, ['stack']);
      }

      throw error;
    }
  };
}
