import { KoaContext, KoaNext, KoaMiddleware } from '../types';

export function initServerTiming (): KoaMiddleware {
  return async (ctx: KoaContext, next: KoaNext) => {
    const start = process.hrtime();

    await next();

    const [sec, nanosec] = process.hrtime(start);
    const duration = (sec * 1000 + nanosec / 1000000).toFixed(2);

    ctx.append('Server-Timing', `total;dur=${duration}`);
  };
}
