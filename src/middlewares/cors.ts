import { KoaContext, KoaNext, KoaMiddleware } from '../types';

const ALLOW_METHODS = 'OPTIONS,GET,PUT,POST,PATCH,DELETE';

export function initCors (): KoaMiddleware {
  return async (ctx: KoaContext, next: KoaNext) => {
    ctx.vary('Origin');

    if (!ctx.get('Origin')) {
      await next();
      return;
    }

    ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Methods', ALLOW_METHODS);
    ctx.set('Access-Control-Allow-Headers', ctx.get('Access-Control-Request-Headers'));

    if (ctx.method === 'OPTIONS') {
      ctx.status = 204;
    } else {
      await next();
    }
  };
}
