import protect from 'koa-protect';
import { KoaMiddleware } from '../types';
import { ILogger } from '../types/interfaces';

interface SqlInjectionOptions {
	logger: ILogger
}

export function initSqlInjectionProtection (options: SqlInjectionOptions): KoaMiddleware {
  return (protect as any).koa.sqlInjection({
    body          : true,
    loggerFunction: options.logger
  });
}
