import _ from 'lodash';
import moment from 'moment';
import prettyMilliseconds from 'pretty-ms';
import { KoaContext, KoaNext, KoaMiddleware } from '../types';
import { IncomingHttpHeaders } from 'http';
import { ILogger } from '../types/interfaces';

import 'moment-timezone';
import { TracedApiException } from '../errors';

interface RequestLoggerOptions {
	blackList?: string[];
	timezone: string;
	logger: ILogger
}

interface RequestLoggerLogData {
	start: string;
	method: string;
	path: string;
	ipAddress?: string;
	status?: number;
	message?: string;
	duration?: string;
	user?: number | null;
	request?: {
		params: Object;
		query: Object;
		body: Object;
		headers: string;
	};
	error?: {};
}

export function initRequestLogger (options: RequestLoggerOptions): KoaMiddleware {
  return async (ctx: KoaContext, next: KoaNext) => {
    const start = Date.now();

    const data: RequestLoggerLogData = {
      start : moment().tz(options.timezone).format('YYYY-MM-DD HH:mm:ss'),
      method: ctx.method,
      path  : ctx.path
    };

    try {
      await next();
    } catch (error) {
      if (error instanceof TracedApiException) {
        data.error = error.toJSON();
      } else {
        data.error = {
          name   : _.get(error, 'name', 'Unknown error'),
          message: _.get(error, 'message', 'Error'),
          stack  : _.get(error, 'stack', 'Empty stack').split('\n')
        };
      }
    } finally {
      data.ipAddress = ctx.request.ip;
      data.status    = ctx.response.status;
      data.message   = ctx.response.message;
      data.duration  = prettyMilliseconds(Date.now() - start);

      data.user = ctx.state.user ? ctx.state.user.id : null;

      data.request = {
        params : prepareLogObject(ctx.params, options.blackList),
        query  : prepareLogObject(ctx.query, options.blackList),
        body   : prepareLogObject(ctx.request.body, options.blackList),
        headers: prepareHeaders(ctx.request.headers)
      };

      options.logger[data.error ? 'error' : 'info'](data);
    }
  };
}

// helpers
function prepareLogObject (body: Record<string, any>, blackList: string[] = []): Record<string, any> {
  const isObject = body instanceof Object && !(body instanceof Array);

  if (!isObject) {
    return body;
  }

  const data = JSON.parse(JSON.stringify(body));

  for (const key of blackList) {
    if (_.get(data, key)) {
      _.set(data, key, '**********');
    }
  }
  return data;
}

function prepareHeaders (headers: IncomingHttpHeaders): string {
  return Object.keys(headers).join(', ');
}
