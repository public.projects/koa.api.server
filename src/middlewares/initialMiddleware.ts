import { KoaContext, KoaNext, KoaMiddleware } from '../types';

export function initInitialMiddleware (name: string, version: string): KoaMiddleware {
  return async function (ctx: KoaContext, next: KoaNext) {
    ctx.append('X-Server-Name', name);
    ctx.append('X-Api-Version', version);

    let { ip } = ctx.request;

    if (ip === '::1') {
      ctx.request.ip = '127.0.0.1';
    }

    await next();
  };
}
