import protect from 'koa-protect';
import { KoaMiddleware } from '../types';
import { ILogger } from '../types/interfaces';

interface XssProtectOptions {
	logger: ILogger
}

export function initXssProtection (options: XssProtectOptions): KoaMiddleware {
  return (protect as any).koa.xss({
    body          : true,
    loggerFunction: options.logger
  });
}
