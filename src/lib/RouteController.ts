import * as koa from 'koa';
import Router from 'koa-router';
import koaBody, { KoaBodyMiddlewareOptions } from 'koa-body';
import * as yup from 'yup';
import { paramsParser, queryParser, normalizePath } from '../utils';
import { BaseExecutionContext, AppAuthenticators } from '../types';

export interface EndpointInputs<Params extends object, Query extends object, Body extends object> {
  params: Params;
  query: Query;
  body: Body;
  ip: string;
}

export interface EndpointAdditionalContext {
  throw: (status: number, message?: string) => void;
  redirect: (url: string) => void;
  originCtx: koa.Context;
}

export interface EndpointValidatorObj<Params extends object, Query extends object, Body extends object> {
  params?: yup.ObjectSchema<Params>;
  query?: yup.ObjectSchema<Query>;
  body?: yup.ObjectSchema<Body>;
}

export type EndpointValidatorFn<Params extends object, Query extends object, Body extends object> =
  (options: EndpointInputs<Params, Query, Body>) => EndpointValidatorObj<Params, Query, Body>;

export type EndpointValidator<Params extends object, Query extends object, Body extends object> =
  EndpointValidatorObj<Params, Query, Body> | EndpointValidatorFn<Params, Query, Body>

export interface EndpointHandler<
  ExecutorContext extends BaseExecutionContext,
  Params extends Object,
  Query extends Object,
  Body extends Object
> {
  (
    this: ExecutorContext & EndpointAdditionalContext,
    inputs: EndpointInputs<Params, Query, Body>,
    state: koa.DefaultState
  ): Promise<any>;
}

interface RouteConfig<
  ExecutorContext extends BaseExecutionContext,
  Params extends Object,
  Query extends Object,
  Body extends Object
> {
  body?          : KoaBodyMiddlewareOptions & { parseBlacklist?: string[] };
  auth?          : string;
  middlewares?   : koa.Middleware[];
  validator?     : EndpointValidator<Params, Query, Body>;
  readableErrors?: boolean;
  handler        : EndpointHandler<ExecutorContext, Params, Query, Body>;
}

interface RouterStoreItem<
  ExecutorContext extends BaseExecutionContext,
  Params extends Object,
  Query extends Object,
  Body extends Object
> {
  get?: RouteConfig<ExecutorContext, Params, Query, Body>;
  put?: RouteConfig<ExecutorContext, Params, Query, Body>;
  post?: RouteConfig<ExecutorContext, Params, Query, Body>;
  patch?: RouteConfig<ExecutorContext, Params, Query, Body>;
  del?: RouteConfig<ExecutorContext, Params, Query, Body>;
}

type RouteStoreSection = keyof RouterStoreItem<any, any, any, any>;

const SCHEMA_OPTIONS = {
  abortEarly: false,
  strict    : true
};

export class RouteController<ExecutorContext extends BaseExecutionContext>{

  name: string;
  routes: Map<string, RouterStoreItem<ExecutorContext, any, any, any>>;
  attached: boolean;

  constructor (name: string) {
    this.name = name;
    this.routes = new Map();
    this.attached = false;
  }

  get<Params extends Object = Object, Query extends Object = Object> (
    path: string, config: RouteConfig<ExecutorContext, Params, Query, Object>
  ): this {
    return this.set('get', path, config);
  }

  post<Params extends Object = Object, Query extends Object = Object, Body extends Object = Object> (
    path: string, config: RouteConfig<ExecutorContext, Params, Query, Body>
  ): this {
    return this.set('post', path, config);
  }

  put<Params extends Object = Object, Query extends Object = Object, Body extends Object = Object> (
    path: string, config: RouteConfig<ExecutorContext, Params, Query, Body>
  ): this {
    return this.set('put', path, config);
  }

  patch<Params extends Object = Object, Query extends Object = Object, Body extends Object = Object> (
    path: string, config: RouteConfig<ExecutorContext, Params, Query, Body>
  ): this {
    return this.set('patch', path, config);
  }

  del<Params extends Object = Object, Query extends Object = Object> (
    path: string, config: RouteConfig<ExecutorContext, Params, Query, Object>
  ): this {
    return this.set('del', path, config);
  }

  attach (root: string, router: Router, context: ExecutorContext, authenticators?: AppAuthenticators) {
    if (this.attached) {
      throw new TypeError('Controller is already attached');
    }
    if (!root) {
      throw new TypeError(`Cannot attach route controller to '${root}' root`);
    }
    if (typeof root !== 'string') {
      throw new TypeError('Route controller root must be a string');
    }

    if (!context) {
      throw new TypeError('Route controller cannot be attached to an undefined context');
    }

    const route = root === '/'
      ? new Router()
      : new Router().prefix(normalizePath(root));

    for (let [subpath, config] of this.routes) {
      if (config.get) {
        route.get(subpath, ...this.buildEndpointHandlers(config.get, context, authenticators));
      }
      if (config.put) {
        route.put(subpath, ...this.buildEndpointHandlers(config.put, context, authenticators));
      }
      if (config.post) {
        route.post(subpath, ...this.buildEndpointHandlers(config.post, context, authenticators));
      }
      if (config.patch) {
        route.patch(subpath, ...this.buildEndpointHandlers(config.patch, context, authenticators));
      }
      if (config.del) {
        route.delete(subpath, ...this.buildEndpointHandlers(config.del, context, authenticators));
      }
    }

    router.use(route.routes(), route.allowedMethods());
  }

  private set<Params extends Object, Query extends Object, Body extends Object> (
    method: RouteStoreSection, path: string, config: RouteConfig<ExecutorContext, Params, Query, Body>
  ): this {
    if (this.attached) {
      throw new TypeError('Controller is already attached');
    }

    const subpath = normalizePath(path);

    const routeConfig = this.routes.get(subpath) || {};

    if (routeConfig[method]) {
      throw new Error(`${method.toUpperCase()} ${subpath} has already been bound to a handler`);
    }

    routeConfig[method] = config;

    this.routes.set(subpath, routeConfig);

    return this;
  }

  private buildEndpointHandlers (
    config: RouteConfig<ExecutorContext, any, any, any>,
    context: ExecutorContext,
    authenticators?: AppAuthenticators
  ): koa.Middleware[] {
    if (!config) {
      throw new TypeError('Controller config or handler is required');
    }

    const handlers = [];

    if (config.auth) {
      if (!authenticators) {
        throw new TypeError('Authenticators is not provided to app context');
      }
      if (!authenticators[config.auth]) {
        throw new TypeError(`Authenticator "${config.auth}" is not provided`);
      }

      const authMiddleware = authenticators[config.auth]();

      handlers.push(authMiddleware);
    }

    if (config.middlewares && config.middlewares.length) {
      handlers.push(...config.middlewares);
    }

    const { parseBlacklist = [], ...bodyConfig } = (config.body || {});

    handlers.push(koaBody(bodyConfig));

    handlers.push(async (ctx: koa.Context) => {
      const inputs = {
        params: paramsParser(ctx.params, parseBlacklist),
        query : queryParser(ctx.query, parseBlacklist),
        body  : ctx.request.body || {},
        ip    : ctx.request.ip
      };

      const validatorObj = typeof config.validator === 'function'
        ? config.validator(inputs)
        : config.validator;

      const validator = {
        params: (validatorObj?.params || yup.object({})).noUnknown(true),
        query : (validatorObj?.query  || yup.object({})).noUnknown(true),
        body  : (validatorObj?.body   || yup.object({})).noUnknown(true)
      };

      try {
        await validator.params.validate(inputs.params, SCHEMA_OPTIONS);
        await validator.query.validate(inputs.query,   SCHEMA_OPTIONS);
        await validator.body.validate(inputs.body,     SCHEMA_OPTIONS);
      } catch (error) {
        context.logger.error(error);

        const readableErrors = typeof config.readableErrors === 'boolean'
          ? config.readableErrors
          : true;

        let errorMessage: string = 'Bad request. Invalid inputs';

        if (readableErrors) {
          if (error instanceof yup.ValidationError) {
            errorMessage = error.errors.join(';');
          }

          context.logger.error(errorMessage);
        }

        ctx.throw(400, errorMessage);
      }

      const executionContext = {
        ...context,
        throw    : ctx.throw.bind(ctx),
        redirect : ctx.redirect.bind(ctx),
        originCtx: ctx
      };

      const result = await config.handler.call(executionContext, inputs, ctx.state);

      if (ctx.status === 302 || typeof ctx.body !== 'undefined') {
        return;
      }
      if (typeof result !== 'undefined') {
        ctx.body = result;
        return;
      }

      ctx.status = 204;
    });

    return handlers;
  }
}
