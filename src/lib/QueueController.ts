import Bull from 'bull';
import { Queue } from './Queue';
import { BaseExecutionContext } from '../types';

export interface QueueHandlerResults {
  processed: boolean;
  reason?: string;
  params?: Record<string, any>;
}

export interface QueueHandlerFn<ExecutorContext extends BaseExecutionContext, JobParams extends Object> {
  (this: ExecutorContext, params: JobParams, job: Bull.Job): Promise<QueueHandlerResults>;
}

interface QueueControllerStoreItem<ExecutorContext extends BaseExecutionContext, JobParams extends Object> {
  handler: QueueHandlerFn<ExecutorContext, JobParams>;
  concurrency: number;
}

export class QueueController<ExecutorContext extends BaseExecutionContext>{

  name: string;
  handlers: Map<string, QueueControllerStoreItem<any, any>>;
  attached: boolean;

  constructor (name: string) {
    this.name = name;
    this.handlers = new Map();
    this.attached = false;
  }

  set<JobParams extends Object = Object> (
    jobName: string, concurrency: number, handler: QueueHandlerFn<ExecutorContext, JobParams>
  ): this {
    if (this.attached) {
      throw new TypeError('Handler is already attached');
    }

    if (this.handlers.has(jobName)) {
      throw new Error(`Handlers for ${jobName} has already been added`);
    }

    this.handlers.set(jobName, { handler, concurrency });

    return this;
  }

  attach (queue: Queue, context: ExecutorContext) {
    if (this.attached) {
      throw new TypeError('Handler is already attached');
    }

    if (!context) {
      throw new TypeError('Queue controller cannot be attached to an undefined context');
    }

    for (const [jobName, { handler, concurrency }] of this.handlers) {
      queue.processJob(jobName, concurrency, this.buildQueueHandler(handler, context));
    }
  }

  private buildQueueHandler (handler: QueueHandlerFn<ExecutorContext, any>, context: ExecutorContext) {
    return async (job: Bull.Job) => {
      return handler.call(context, job.data, job);
    };
  }
}
