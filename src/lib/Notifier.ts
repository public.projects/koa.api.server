import { Server, Socket, Namespace } from 'socket.io';
import http from 'http';
import { ILogger } from '../types/interfaces';

const CONNECTION_ERROR_MESSAGE = 'Authentication error';

export type IoSocket = Socket;

interface SocketUser {
	id: number | string;
}

interface AuthMiddlewareFn {
	(socket: Socket): Promise<{ user: null | SocketUser }>
}

interface NotifierNamespace {
  name: string,
  middleware?: AuthMiddlewareFn
}

export interface NotifierOptions {
	namespaces: NotifierNamespace[];
	logger: ILogger;
}

type SocketStore = Record<string, Socket[]>

export class Notifier {

  private logger: ILogger;
  private io: Server | null;
  private store: SocketStore;
  private config: NotifierNamespace[];
  private namespaces: Record<string, Namespace>;

  constructor (options: NotifierOptions) {
    const { namespaces, logger } = options;

    this.io         = null;
    this.logger     = logger;
    this.store      = {};
    this.namespaces = {};

    this.config = namespaces;
  }

  connect (server: http.Server) {
    this.io = new Server(server, {
      cors: { origin: '*' } // todo fix me later
    });

    for (const { name, middleware } of this.config) {
      this.store[name] = [];

      const namespace = this.io!.of(name);

      if (middleware) {
        namespace.use(async (socket: Socket, next) => {
          const { user } = await middleware!(socket);

          if (!user) {
            next(new Error(CONNECTION_ERROR_MESSAGE));
            return;
          }

          socket.data.user = user as SocketUser;

          next();
        });
      }

      namespace.on('connection', (socket: Socket) => {
        const { user } = socket.data;

        this.logger.info(`${name}: connected user with ID ${user.id}`);

        this.store[name].push(socket);

        socket.on('disconnect', () => {
          const index = this.store[name].indexOf(socket);

          this.store[name].splice(index, 1);

          this.logger.info(`${name}: disconnected user with ID ${user.id}`);
        });
      });

      this.namespaces[name] = namespace;
    }
  }

  emitTo<T> (namespace: string, userId: number, event: string, data: T): void {
    this.store[namespace].forEach((socket) => {
      if (socket.data.user.id === userId) {
        socket.emit(event, data);
      }
    });
  }

  connectedIds (namespace: string): number[] {
    return this.store[namespace]
      .map((socket) => socket.data.user.id)
      .filter((id, index, self) => self.indexOf(id) === index)
      .sort((id1, id2) => id1 - id2);
  }

  connectionCount (namespace: string, userId: number) {
    return this.store[namespace]
      .reduce((count, socket) => (socket.data.user.id === userId ? count + 1 : count), 0);
  }
}
