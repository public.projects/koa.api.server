import _ from 'lodash';
import Bull from 'bull';
import * as Redis from 'ioredis';
import { ILogger } from '../types/interfaces';

export interface QueueLogConfig {
	logEvents?: boolean;
  logJobAddedAction?: boolean;
  logJobRemovedAction?: boolean;
  logStartedEvents?: boolean;
  logCompletedEvents?: boolean;
  logFailedEvents?: boolean;
  logEventBlackList?: string[];
  logDataBlackList?: string[];
}

export type QueueRedisOptions = Redis.RedisOptions;
export type QueueRateLimiter = Bull.RateLimiter;
export type QueueJobOptions = Bull.JobOptions;
export type QueueAdvancedSettings = Bull.AdvancedSettings;

export interface QueueOptions {
	name: string;
	logger: ILogger;
	redis: QueueRedisOptions;
	limiter?: QueueRateLimiter;
	jobOptions?: QueueJobOptions;
	advancedSettings?: QueueAdvancedSettings;
	config?: QueueLogConfig;
}

type JobResult = Object | string | undefined;

export type JobHandler<T> = Bull.ProcessPromiseFunction<T> | Bull.ProcessCallbackFunction<T>;

const DEFAULT_CONFIG: Required<QueueLogConfig> = {
  logEvents          : true,
  logJobAddedAction  : true,
  logJobRemovedAction: true,
  logStartedEvents   : true,
  logCompletedEvents : true,
  logFailedEvents    : true,
  logEventBlackList  : [],
  logDataBlackList   : []
};

const DEFAULT_LIMITER: Bull.RateLimiter = {
  max       : 1000, // Max number of jobs processed
  duration  : 5000, // per duration in milliseconds
  bounceBack: false // When jobs get rate limited, they stay in the waiting queue and are not moved to the delayed queue
};

const DEFAULT_JOB_OPTIONS: Bull.JobOptions = {
  removeOnComplete: true, // If true, removes the job when it successfully
  removeOnFail    : true  // If true, removes the job when it fails after all attempts
};

const DEFAULT_ADVANCED_SETTINGS: Bull.AdvancedSettings = {
  lockDuration     : 30000, // Key expiration time for job locks.
  lockRenewTime    : 15000, // Interval on which to acquire the job lock
  stalledInterval  : 30000, // How often check for stalled jobs (use 0 for never checking).
  maxStalledCount  : 1,     // Max amount of times a stalled job will be re-processed.
  guardInterval    : 5000,  // Poll interval for delayed jobs and added jobs.
  retryProcessDelay: 5000,  // delay before processing next job in case of internal error.
  backoffStrategies: {},    // A set of custom backoff strategies keyed by name.
  drainDelay       : 5      // A timeout for when the queue is in drained state (empty waiting for jobs).
};

export class Queue {

  private logger: ILogger;
  readonly bull: Bull.Queue;
  private config: Required<QueueLogConfig>;

  constructor (options: QueueOptions) {
    this.logger = options.logger;

    this.config = {
      ...DEFAULT_CONFIG,
      ...options.config
    };

    this.bull = new Bull(options.name, {
      redis: options.redis,
      limiter: {
        ...DEFAULT_LIMITER,
        ...options.limiter
      },
      defaultJobOptions: {
        ...DEFAULT_JOB_OPTIONS,
        ...options.jobOptions
      },
      settings: {
        ...DEFAULT_ADVANCED_SETTINGS,
        ...options.advancedSettings
      }
    });

    this.bull.on('active',    this.activeLogger.bind(this));
    this.bull.on('completed', this.successLogger.bind(this));
    this.bull.on('failed',    this.failedLogger.bind(this));
    this.bull.on('error',     this.errorLogger.bind(this));
  }

  close (): void {
    this.bull.close();
  }

  private formJobName (job: Bull.Job): string {
    const data = prepareLogObject(job.data, this.config.logDataBlackList);

    return `'${job.name}' (${JSON.stringify(data)})`;
  }

  private activeLogger (job: Bull.Job): void {
    const { logEvents, logStartedEvents, logEventBlackList } = this.config;

    if (logEvents && logStartedEvents && logEventBlackList.indexOf(job.name) === -1) {
      this.logger.info(`Job ${this.formJobName(job)} started`);
    }
  }

  private successLogger (job: Bull.Job, result: JobResult): void {
    const { logEvents, logCompletedEvents, logEventBlackList } = this.config;

    if (logEvents && logCompletedEvents && logEventBlackList.indexOf(job.name) === -1) {
      const data = prepareLogObject(result, this.config.logDataBlackList);
      this.logger.info(`Job ${this.formJobName(job)} completed with result ${JSON.stringify(data)}`);
    }
  }

  private failedLogger (job: Bull.Job, err: Error) {
    const { logEvents, logFailedEvents } = this.config;

    if (logEvents && logFailedEvents) {
      this.logger.error(`Job ${this.formJobName(job)} failed with error ${err.message}`);
    }
  }

  private errorLogger (err: Error) {
    this.logger.error(err);
  }

  async addJob<T extends object> (
    jobName: string,
    data: T = {} as T,
    options: Bull.JobOptions = {}
  ): Promise<void> {
    const jobOptions = { ...options };

    if (!jobOptions.attempts) {
      jobOptions.attempts = 3;
    }
    if (!jobOptions.backoff) {
      jobOptions.backoff = 5000;
    }

    await this.bull.add(jobName, data, jobOptions);

    const { logEvents, logJobAddedAction, logEventBlackList } = this.config;

    if (logEvents && logJobAddedAction && logEventBlackList.indexOf(jobName) === -1) {
      this.logger.info({
        type: 'JOB ADDED',
        job : jobName,
        data: prepareLogObject(data, this.config.logDataBlackList),
        jobOptions
      });
    }
  }

  async addRepeatableJob (jobName: string, data: Object, cron: string): Promise<void> {
    await this.removeRepeatableJob(jobName);

    await this.bull.add(jobName, data, { repeat: { cron } });

    const { logEvents, logJobAddedAction, logEventBlackList } = this.config;

    if (logEvents && logJobAddedAction && logEventBlackList.indexOf(jobName) === -1) {
      this.logger.info({
        type: 'JOB ADDED REP',
        job : jobName,
        data: prepareLogObject(data, this.config.logDataBlackList),
        cron
      });
    }
  }

  async removeRepeatableJob (jobName: string): Promise<void> {
    const jobs = await this.bull.getRepeatableJobs();
    const curr = jobs.filter((job) => job.key.search(jobName) > -1);

    for (let i = 0; i < curr.length; i++) {
      await this.bull.removeRepeatableByKey(curr[i].key);
    }
  }

  processJob<T> (jobName: string, concurrency: number, handler: JobHandler<T>): void {
    this.bull.process(jobName, concurrency, handler);
  }

  async removeJob (jobName: string, params: Record<string, any>): Promise<void> {
    const { client, name } = this.bull;

    const keys = await client.keys(`bull:${name}:*`);

    for (let i = 0; i < keys.length; i++) {
      const key   = keys[i];
      const parts = key.split(':');
      const jobId = parts[2];

      if (parts.length === 3 && jobId.search(/^\d+$/) === 0) {
        const job = await this.bull.getJob(jobId);

        if (job && job.name === jobName) {
          if (Object.keys(params).every((k) => params[k] === job.data[k])) {
            await job.remove();

            const { logEvents, logJobRemovedAction, logEventBlackList } = this.config;

            if (logEvents && logJobRemovedAction && logEventBlackList.indexOf(jobName) === -1) {
              this.logger.info({ type: 'JOB REMOVED', job: jobName, params });
            }
          }
        }
      }
    }
  }
}

// helpers
function prepareLogObject (body: any, blackList: string[] = []): Record<string, any> {
  const isObject = body instanceof Object && !(body instanceof Array);

  if (!isObject) {
    return body;
  }

  const data = JSON.parse(JSON.stringify(body));

  for (const key of blackList) {
    if (_.get(data, key)) {
      _.set(data, key, '**********');
    }
  }

  return data;
}
