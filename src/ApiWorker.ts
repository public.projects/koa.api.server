import * as events from 'events';
import prettyMs from 'pretty-ms';
import { QueueController } from './lib';
import { BaseExecutionContext, ContextQueues } from './types';
import { ILogger } from './types/interfaces';

const { version: VERSION } = require('../../package.json');

import 'moment-timezone';

enum WorkerState {
  Created = 0,
  Initiated = 1,
  Started = 2,
  Stopped = 3
}

type BaseWorkerExecutionContext = BaseExecutionContext & {
  readonly queue: ContextQueues;
}

export interface WorkerOptions {
  name: string;
  version: string;
  timezone: string;
  isDevEnv: boolean;
  logger: ILogger;
}

export interface WorkerInitOptions<WorkerExecutionContext extends BaseWorkerExecutionContext> {
  context: WorkerExecutionContext;
}

export interface JobHandlerOptions {
  concurrency: number;
}

export class ApiWorker<ExecutionContext extends BaseWorkerExecutionContext> extends events.EventEmitter {

  private state: WorkerState = WorkerState.Created;
  readonly createdAt: Date = new Date();
  readonly name: string = 'koa.api.worker';
  readonly version: string = VERSION;
  readonly isDevEnv: boolean;
  readonly prefix: string;
  readonly timezone: string;

  private readonly logger: ILogger;

  private context?: ExecutionContext;

  private queueControllers: Map<string, QueueController<ExecutionContext>> = new Map();

  constructor (options: WorkerOptions) {
    super();

    this.setMaxListeners(100);

    this.prefix   = options.name;
    this.version  = options.version;
    this.timezone = options.timezone;
    this.isDevEnv = options.isDevEnv;

    this.logger = options.logger;
  }

  init (options: WorkerInitOptions<ExecutionContext>) {
    if (!this.isCreated) {
      throw new TypeError(`Cannot init worker: server is in "${this.state}" state`);
    }
    if (!options.context) {
      throw new TypeError('Context required');
    }
    if (!options.context.queue) {
      throw new TypeError('Queue required');
    }

    this.context = options.context;

    this.state = WorkerState.Initiated;

    this.dispatch('initialized');
  }

  async start () {
    if (!this.isInitiated) {
      throw new TypeError('Cannot start worker: worker is not initiated');
    }
    if (!this.queueControllers.size) {
      throw new TypeError('Cannot start worker: no handlers has been registered');
    }

    this.dispatch('starting');

    this.attachQueueControllers();

    this.logger.info(`Time: ${new Date()}`);
    this.logger.info(`Launched in: ${prettyMs(Date.now() - Number(this.createdAt))}`);
    this.logger.info(`Environment: ${this.context!.env}`);
    this.logger.info(`Process PID: ${process.pid}`);
    this.logger.info(`Engine: version: ${VERSION} (node v${process.versions.node})`);
    this.logger.info(`App version: ${this.version}\n`);

    if (this.isDevEnv) {
      this.logger.info('To shut down your worker, press <CTRL> + C at any time\n');
    }

    this.state = WorkerState.Started;

    this.dispatch('started');
  }

  async stop () {
    if (!this.isStarted) {
      throw new TypeError('Cannot stop worker: worker has not been started');
    }

    if (this.context && this.context.queue) {
      for (const queueName in this.context.queue) {
        this.context.queue[queueName].close();
      }
    }

    this.state = WorkerState.Stopped;

    this.dispatch('stopped');
  }

  register (queueName: string, controller: QueueController<ExecutionContext>): void {
    if (!this.isCreated && !this.isInitiated) {
      throw new TypeError(`Cannot register queue controller: worker is in "${this.state}" state`);
    }

    if (!this.context || !this.context.queue || !this.context.queue[queueName]) {
      throw TypeError(`Cannot register queue controller: Queue ${queueName} is absent in context`);
    }

    if (!controller) {
      throw new TypeError('Cannot register queue controller: controller is undefined');
    }

    if (this.queueControllers.has(queueName)) {
      throw TypeError(`Controller for '${queueName}' queue has already been registered`);
    }

    this.queueControllers.set(queueName, controller);

    this.dispatch(`queue:${queueName}:registered`);
  }

  dispatch (event: string, message?: string | Object | Error): void {
    const eventName = `${this.prefix}:${event}`;

    this.logger.info(`🔔 ${eventName}`);
    this.emit(eventName, message);
  }

  private get isCreated () {
    return this.state === WorkerState.Created;
  }

  private get isInitiated () {
    return this.state === WorkerState.Initiated;
  }

  private get isStarted () {
    return this.state === WorkerState.Started;
  }

  private attachQueueControllers () {
    for (const [queueName, controller] of this.queueControllers) {
      if (this.context && this.context.queue && this.context.queue[queueName]) {
        controller.attach(this.context.queue[queueName], this.context);
        this.logger.info(`⚡️ ${queueName} queue: attached controller`);
      }
    }
  }
}
