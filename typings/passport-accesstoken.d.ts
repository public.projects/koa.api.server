/* eslint-disable no-mixed-spaces-and-tabs */
declare module 'passport-accesstoken' {
	import { ClientRequest } from 'http';

	interface StrategyOptions {
		tokenHeader?: string;
		tokenField?: string;
		tokenQuery?: string;
	}

	export type verifiedFn = (err: Error | string | null, user: Object | boolean | null, info?: any) => void;

	export interface StrategyVerifyFn {
		(req: ClientRequest, token: string, verified: verifiedFn): void | Promise<void>;
		(token: string, verified: verifiedFn): void | Promise<void>;
	}

	export class Strategy {
	  _tokenHeader: string;

	  _tokenField: string;

	  _tokenQuery: string;

	  _verify: StrategyVerifyFn;

	  _passReqToCallback: boolean;

	  name: string;

	  constructor (options: StrategyOptions, verify: StrategyVerifyFn): Strategy;

	  constructor (verify: StrategyVerifyFn): Strategy;
	}
}
